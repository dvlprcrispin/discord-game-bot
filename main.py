import discord
import asyncio
import config
import sys
from aiohttp import web

client = discord.Client()

# discord stuff

@client.event
async def on_ready():
    print('Discord running')
    print(client.user.name)
    print(client.user.id)
    print('------')

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content.startswith("> ping"):
        await message.channel.send("Pong!")
    if message.content.startswith("> help"):
        await message.channel.send("There is no help.")

# web

async def handle_web(request):
    try:
        data = await request.json()
        url = data["url"]
        name = data["name"]
        commit = data["commit"]
    except KeyError:
        print("Someone put in the wrong thing")
    channel = client.get_channel(config.channel_id)
    message = discord.Embed(type="rich", title=f"New update at {name}", url=url, description=commit)
    message.set_thumbnail(url=f"{url}favicon.ico")
    message.set_image(url=f"{url}favicon.ico")
    await channel.send(" ", embed=message)
    return web.Response(text="All good.")


async def web_main(loop):
    """help"""
    app = web.Application(loop=loop)

    runner = web.AppRunner(app)
    app.router.add_post('/', handle_web)
    await runner.setup()
    
    site = web.TCPSite(runner, '0.0.0.0', 8080)
    await site.start()
    print("HTTP running")
    # await runner.cleanup()



if __name__ == "__main__":
    event_loop = asyncio.get_event_loop()
    event_loop.run_until_complete(web_main(event_loop))
    event_loop.run_until_complete(client.run(config.very_secret_key))
    
    try:
        event_loop.run_forever()
    except KeyboardInterrupt:
        print("dirty close")
        event_loop.close()
    finally:
        print("closing")
        event_loop.close()
